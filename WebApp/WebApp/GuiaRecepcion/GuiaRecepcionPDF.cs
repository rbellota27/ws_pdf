﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using Entidades;
using Negocio;
using System.Text;
using iTextSharp.text.pdf.parser;
using System.Data;
using iTextSharp.text.html.simpleparser;

namespace WcfServicioPDF.GuiaRecepcion
{
    public class GuiaRecepcionPDF
    {
        public static object PDFParser { get; private set; }

        public string GenerateInvoice(string TransactionID)
        {
            var output = new MemoryStream();
            Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 30f, 0f);

            // Open the Document for writing
            MemoryStream PdfData = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, PdfData);
            pdfDoc.Open();


            Negocio.DocumentosMercantiles reporte = new Negocio.DocumentosMercantiles();
            List<Entidades.Documento> cabecera = new List<Entidades.Documento>();
            cabecera = reporte.CabeceraGuiaRemision2_(Convert.ToInt32(TransactionID));
            List<Entidades.DetalleDocumento> detallesGuiaRemision = new List<Entidades.DetalleDocumento>();
            detallesGuiaRemision = reporte.DetallesGuiaRemision(Convert.ToInt32(TransactionID));

            Entidades.Documento observaciones = new Entidades.Documento();
            observaciones = reporte.ObservacionesGuiaRemision(Convert.ToInt32(TransactionID));
            string NumDocumento = cabecera[0].NroDocumento;

            var titleFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
            var titleFontBlue = FontFactory.GetFont("Arial", 14, Font.NORMAL, BaseColor.BLUE);
            var boldTableFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
            var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var bodyfuente = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var itemtablefuente = FontFactory.GetFont("ArialNarrow", 8, Font.NORMAL);
            var bodyfuentebold = FontFactory.GetFont("Verdana", 10, Font.BOLD);
            var fontdireccion = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var EmailFont = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLUE);
            var footblanco = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.WHITE);

            var piepag = FontFactory.GetFont("Arial", 8, Font.NORMAL);

            BaseColor TabelHeaderBackGroundColor = WebColors.GetRGBColor("#EEEEEE");

            PdfPTable headertable = new PdfPTable(3);
            headertable.HorizontalAlignment = 0;
            headertable.WidthPercentage = 100;
            headertable.SetWidths(new float[] { 130f, 40f, 110f });  // then set the column's __relative__ widths             
            headertable.DefaultCell.Border = Rectangle.NO_BORDER;

            ////IMAGEN DE LOGO
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/imagenes/logo.png"));
            logo.ScaleToFit(155, 110);
            PdfPCell pdfCelllogo = new PdfPCell(logo);
            pdfCelllogo.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
            //pdfCelllogo.Border = Rectangle.BOX;                
            pdfCelllogo.HorizontalAlignment = 0;
            pdfCelllogo.PaddingTop = 5f;
            pdfCelllogo.PaddingLeft = 20f;

            headertable.AddCell(pdfCelllogo);


            {
                PdfPCell middlecell = new PdfPCell();
                middlecell.Border = Rectangle.TOP_BORDER;
                headertable.AddCell(middlecell);
            }

            {
                PdfPTable nested = new PdfPTable(1);
                nested.DefaultCell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                nested.WidthPercentage = 9;
                nested.HorizontalAlignment = 0;
                pdfCelllogo.PaddingLeft = 25f;
                nested.SetWidths(new float[] { 1f });

                PdfPCell nextPostCell1 = new PdfPCell(new Phrase("RUC", titleFont));
                nextPostCell1.Border = Rectangle.BOX;
                nextPostCell1.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                nextPostCell1.BorderWidthTop = 1f;
                nextPostCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                nested.AddCell(nextPostCell1);

                PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Guia Recepcion", titleFont));
                nextPostCell2.Border = Rectangle.BOX;
                nextPostCell2.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                nextPostCell2.BorderWidthTop = 1f;
                nextPostCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                nested.AddCell(nextPostCell2);


                PdfPCell nextPostCell3 = new PdfPCell(new Phrase(NumDocumento, boldTableFont));
                nextPostCell3.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
                nextPostCell3.BorderWidthTop = 1f;
                nextPostCell3.Border = Rectangle.BOX;
                nextPostCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                nested.AddCell(nextPostCell3);

                //    //CUADRO DEBAJO DEL LOGO
                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                nesthousing.PaddingRight = 5f;
                nesthousing.PaddingTop = 5f;
                nesthousing.PaddingLeft = 45f;
                nesthousing.PaddingBottom = 2f;

                headertable.AddCell(nesthousing);
            }

            pdfDoc.Add(headertable);


            PdfPTable tabladireccion = new PdfPTable(2);
            tabladireccion.HorizontalAlignment = 0;
            tabladireccion.WidthPercentage = 100;
            tabladireccion.SetWidths(new float[] { 90f, 188f });  // then set the column's __relative__ widths
            tabladireccion.DefaultCell.Border = Rectangle.NO_BORDER;

            {
                PdfPCell cellcabdirecsani = new PdfPCell(new Phrase(cabecera[0].DireccionProp, fontdireccion));
                cellcabdirecsani.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdirecsani.Border = Rectangle.LEFT_BORDER;
                cellcabdirecsani.PaddingLeft = 20f;
                tabladireccion.AddCell(cellcabdirecsani);

                PdfPCell cellcabdirecsani0 = new PdfPCell(new Phrase("", bodyFont));
                cellcabdirecsani0.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdirecsani0.Border = Rectangle.RIGHT_BORDER;
                tabladireccion.AddCell(cellcabdirecsani0);
            }


            pdfDoc.Add(tabladireccion);

            PdfPTable tablaprincipal = new PdfPTable(4);
            tablaprincipal.HorizontalAlignment = 0;
            tablaprincipal.WidthPercentage = 100;
            tablaprincipal.SpacingAfter = 2;
            tablaprincipal.SetWidths(new float[] { 20, 55,20,15 });  // then set the column's __relative__ widths
            tablaprincipal.DefaultCell.Border = Rectangle.BOX;

            {


                PdfPCell cellcabfe = new PdfPCell(new Phrase("Fecha:", bodyfuentebold));
                cellcabfe.BackgroundColor = BaseColor.WHITE;
                cellcabfe.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabfe.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcabfe);

                PdfPCell cellcabfeBD = new PdfPCell(new Phrase(cabecera[0].getFechaEmisionText, bodyFont));
                cellcabfeBD.BackgroundColor = BaseColor.WHITE;
                cellcabfeBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabfeBD.Border = Rectangle.RIGHT_BORDER;
                cellcabfeBD.Colspan = 3;
                tablaprincipal.AddCell(cellcabfeBD);



                PdfPCell cellcabusu = new PdfPCell(new Phrase("Usuario:", bodyfuentebold));
                cellcabusu.BackgroundColor = BaseColor.WHITE;
                cellcabusu.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabusu.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcabusu);

                PdfPCell cellcabusuBD = new PdfPCell(new Phrase(cabecera[0].Usuario, bodyFont));
                cellcabusuBD.BackgroundColor = BaseColor.WHITE;
                cellcabusuBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabusuBD.Border = Rectangle.RIGHT_BORDER;
                cellcabusuBD.Colspan = 3;
                tablaprincipal.AddCell(cellcabusuBD);


                PdfPCell cellcabremi = new PdfPCell(new Phrase("Remitente:", bodyfuentebold));
                cellcabremi.BackgroundColor = BaseColor.WHITE;
                cellcabremi.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabremi.Border = Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
                tablaprincipal.AddCell(cellcabremi);

                PdfPCell cellcabremiBD = new PdfPCell(new Phrase("", bodyFont));
                cellcabremiBD.BackgroundColor = BaseColor.WHITE;
                cellcabremiBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabremiBD.Border = Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER | Rectangle.TOP_BORDER;
                cellcabremiBD.Colspan = 3;
                tablaprincipal.AddCell(cellcabremiBD);
            }
            {
                PdfPCell cellcabsr = new PdfPCell(new Phrase("Señores:", bodyfuentebold));
                cellcabsr.BackgroundColor = BaseColor.WHITE;
                cellcabsr.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabsr.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcabsr);

                PdfPCell cellacabsrBD = new PdfPCell(new Phrase(cabecera[0].senores, bodyFont));
                cellacabsrBD.BackgroundColor = BaseColor.WHITE;
                cellacabsrBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellacabsrBD.Border = Rectangle.NO_BORDER;
                cellacabsrBD.Colspan = 1;
                tablaprincipal.AddCell(cellacabsrBD);

                PdfPCell cellfactpro = new PdfPCell(new Phrase("Factura Proveedor:", bodyfuentebold));
                cellfactpro.BackgroundColor = BaseColor.WHITE;
                cellfactpro.HorizontalAlignment = Element.ALIGN_LEFT;
                cellfactpro.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(cellfactpro);

                PdfPCell cellfactproBD = new PdfPCell(new Phrase(cabecera[0].fact_proveedor_grc, bodyFont));
                cellfactproBD.BackgroundColor = BaseColor.WHITE;
                cellfactproBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellfactproBD.Border = Rectangle.RIGHT_BORDER;
                cellfactproBD.Colspan = 1;
                tablaprincipal.AddCell(cellfactproBD);
            }
            {
                PdfPCell cellcabruc = new PdfPCell(new Phrase("Ruc:", bodyfuentebold));
                cellcabruc.BackgroundColor = BaseColor.WHITE;
                cellcabruc.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabruc.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcabruc);

                PdfPCell cellcabrucBD = new PdfPCell(new Phrase(cabecera[0].RucClie, bodyFont));
                cellcabrucBD.BackgroundColor = BaseColor.WHITE;
                cellcabrucBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabrucBD.Border = Rectangle.NO_BORDER;
                cellcabrucBD.Colspan = 1;
                tablaprincipal.AddCell(cellcabrucBD);


                PdfPCell callgrmprov = new PdfPCell(new Phrase("GRM Proveedor:", bodyfuentebold));
                callgrmprov.BackgroundColor = BaseColor.WHITE;
                callgrmprov.HorizontalAlignment = Element.ALIGN_LEFT;
                callgrmprov.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(callgrmprov);

                PdfPCell callgrmprovBD = new PdfPCell(new Phrase(cabecera[0].grm_proveedor_grc, bodyFont));
                callgrmprovBD.BackgroundColor = BaseColor.WHITE;
                callgrmprovBD.HorizontalAlignment = Element.ALIGN_LEFT;
                callgrmprovBD.Border = Rectangle.RIGHT_BORDER;
                callgrmprovBD.Colspan = 1;
                tablaprincipal.AddCell(callgrmprovBD);

            }
            {

                PdfPCell cellcabtelef = new PdfPCell(new Phrase("Telf:", bodyfuentebold));
                cellcabtelef.BackgroundColor = BaseColor.WHITE;
                cellcabtelef.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabtelef.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcabtelef);

                PdfPCell cellcabtelefBD = new PdfPCell(new Phrase(cabecera[0].TelfProp, bodyFont));
                cellcabtelefBD.BackgroundColor = BaseColor.WHITE;
                cellcabtelefBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabtelefBD.Border = Rectangle.NO_BORDER;
                cellcabtelefBD.Colspan = 1;
                tablaprincipal.AddCell(cellcabtelefBD);


                PdfPCell callgrmprov = new PdfPCell(new Phrase("OC Referenciada:", bodyfuentebold));
                callgrmprov.BackgroundColor = BaseColor.WHITE;
                callgrmprov.HorizontalAlignment = Element.ALIGN_LEFT;
                callgrmprov.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(callgrmprov);

                PdfPCell callgrmprovBD = new PdfPCell(new Phrase(cabecera[0].num_oc, bodyFont));
                callgrmprovBD.BackgroundColor = BaseColor.WHITE;
                callgrmprovBD.HorizontalAlignment = Element.ALIGN_LEFT;
                callgrmprovBD.Border = Rectangle.RIGHT_BORDER;
                callgrmprovBD.Colspan = 1;
                tablaprincipal.AddCell(callgrmprovBD);
            }
            { 
                PdfPCell cellcabdir = new PdfPCell(new Phrase("Dirección:", bodyfuentebold));
                cellcabdir.BackgroundColor = BaseColor.WHITE;
                cellcabdir.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdir.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER;
                //cellcabdir.Border = Rectangle.BOTTOM_BORDER;
                tablaprincipal.AddCell(cellcabdir);

                PdfPCell cellcabdirBD = new PdfPCell(new Phrase(cabecera[0].Direccion, bodyfuentebold));
                cellcabdirBD.BackgroundColor = BaseColor.WHITE;
                cellcabdirBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdirBD.Border = Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                cellcabdirBD.Colspan = 3;
                tablaprincipal.AddCell(cellcabdirBD);
            }


            pdfDoc.Add(tablaprincipal);

            PdfPTable itemTable = new PdfPTable(6);

            itemTable.HorizontalAlignment = 0;
            itemTable.WidthPercentage = 100;
            itemTable.SetWidths(new float[] { 6, 12, 70, 5, 10, 10 });  // then set the column's __relative__ widths
            itemTable.SpacingAfter = 7;
            itemTable.PaddingTop = 2f;
            itemTable.DefaultCell.Border = Rectangle.BOX;
            PdfPCell cell1 = new PdfPCell(new Phrase("Item", boldTableFont));
            cell1.BackgroundColor = TabelHeaderBackGroundColor;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            itemTable.AddCell(cell1);
            PdfPCell cell2 = new PdfPCell(new Phrase("Codigo", boldTableFont));
            cell2.BackgroundColor = TabelHeaderBackGroundColor;
            cell2.HorizontalAlignment = 1;
            itemTable.AddCell(cell2);
            PdfPCell cell3 = new PdfPCell(new Phrase("Descripcion", boldTableFont));
            cell3.BackgroundColor = TabelHeaderBackGroundColor;
            cell3.HorizontalAlignment = Element.ALIGN_LEFT;
            itemTable.AddCell(cell3);
            PdfPCell cell4 = new PdfPCell(new Phrase("UM", boldTableFont));
            cell4.BackgroundColor = TabelHeaderBackGroundColor;
            cell4.HorizontalAlignment = Element.ALIGN_LEFT;
            itemTable.AddCell(cell4);
            PdfPCell cell5 = new PdfPCell(new Phrase("Cantidad", boldTableFont));
            cell5.BackgroundColor = TabelHeaderBackGroundColor;
            cell5.HorizontalAlignment = Element.ALIGN_RIGHT;
            itemTable.AddCell(cell5);
            PdfPCell cell6 = new PdfPCell(new Phrase("Peso", boldTableFont));
            cell6.BackgroundColor = TabelHeaderBackGroundColor;
            cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
            itemTable.AddCell(cell6);
            int indice = 0;
            foreach (Entidades.DetalleDocumento row in detallesGuiaRemision)
            {
                indice = indice + 1;
                PdfPCell numberCell = new PdfPCell(new Phrase(Convert.ToString(indice), itemtablefuente));
                numberCell.HorizontalAlignment = Element.ALIGN_RIGHT; ;
                numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                itemTable.AddCell(numberCell);

                PdfPCell Codigocell = new PdfPCell(new Phrase(row.CodigoProducto, itemtablefuente));
                Codigocell.HorizontalAlignment = Element.ALIGN_RIGHT;
                Codigocell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(Codigocell);

                PdfPCell descCell = new PdfPCell(new Phrase(row.NomProducto, itemtablefuente));
                descCell.HorizontalAlignment = Element.ALIGN_LEFT;
                descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(descCell);

                PdfPCell qtyCell = new PdfPCell(new Phrase(row.UmPeso, itemtablefuente));
                qtyCell.HorizontalAlignment = Element.ALIGN_LEFT;
                qtyCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(qtyCell);
                PdfPCell amountCell = new PdfPCell(new Phrase(Convert.ToString(Convert.ToDecimal(row.Cantidad)), itemtablefuente));
                amountCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(amountCell);
                PdfPCell totalamtCell = new PdfPCell(new Phrase(row.cadenaUM, itemtablefuente));
                totalamtCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(totalamtCell);
            }
            pdfDoc.Add(itemTable);


            PdfPTable Observaciones = new PdfPTable(1);
            Observaciones.HorizontalAlignment = 0;
            Observaciones.WidthPercentage = 100;
            Observaciones.SetWidths(new float[] { 320f });  // then set the column's __relative__ widths
            Observaciones.DefaultCell.Border = Rectangle.BOX;


            PdfPCell ccellcab = new PdfPCell(new Phrase("Observaciones", boldTableFont));
            ccellcab.BackgroundColor = BaseColor.WHITE;
            ccellcab.HorizontalAlignment = Element.ALIGN_CENTER;
            ccellcab.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
            ccellcab.BorderWidthTop = .8f;
            ccellcab.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
            ccellcab.BorderWidthBottom = .8f;
            ccellcab.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
            ccellcab.BorderWidthRight = .8f;
            ccellcab.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
            ccellcab.BorderWidthLeft = .8f;
            Observaciones.AddCell(ccellcab);
            //TRAER OBSERVACIONES DEL DOCUMENTO
            {
                if (observaciones.observacion == "" || observaciones.observacion == null)
                {
                    PdfPCell cellitemobser = new PdfPCell(new Phrase("..................", bodyfuente));
                    cellitemobser.HorizontalAlignment = Element.ALIGN_CENTER;
                    Observaciones.AddCell(cellitemobser);

                    PdfPCell cellitemobser1 = new PdfPCell(new Phrase("..................", bodyfuente));
                    cellitemobser1.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellitemobser1.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    Observaciones.AddCell(cellitemobser1);
                }
                else
                {
                    PdfPCell cellitemobser = new PdfPCell(new Phrase(observaciones.observacion, bodyfuente));
                    cellitemobser.HorizontalAlignment = Element.ALIGN_CENTER;
                    Observaciones.AddCell(cellitemobser);
                }
            }

            pdfDoc.Add(Observaciones);

            PdfPTable Conformidad = new PdfPTable(1);
            Conformidad.HorizontalAlignment = 0;
            Conformidad.WidthPercentage = 100;
            Conformidad.SetWidths(new float[] { 320f });  // then set the column's __relative__ widths
            Conformidad.DefaultCell.Border = Rectangle.BOX;
            PdfPCell ccellcabconf = new PdfPCell(new Phrase("Conformidad", boldTableFont));
            ccellcabconf.BackgroundColor = BaseColor.WHITE;
            ccellcabconf.HorizontalAlignment = Element.ALIGN_CENTER;
            ccellcabconf.BorderColorTop = new BaseColor(System.Drawing.Color.Black);
            ccellcabconf.BorderWidthTop = .8f;
            ccellcabconf.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
            ccellcabconf.BorderWidthBottom = .8f;
            ccellcabconf.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
            ccellcabconf.BorderWidthRight = .8f;
            ccellcabconf.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
            ccellcabconf.BorderWidthLeft = .8f;


            Conformidad.AddCell(ccellcabconf);
            {
                PdfPCell cellitemenblanco = new PdfPCell(new Phrase("1", footblanco));
                cellitemenblanco.HorizontalAlignment = Element.ALIGN_LEFT;
                cellitemenblanco.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                cellitemenblanco.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
                cellitemenblanco.BorderWidthLeft = .8f;

                cellitemenblanco.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
                cellitemenblanco.BorderWidthRight = .8f;


                Conformidad.AddCell(cellitemenblanco);

                PdfPCell cellitemenblanco2 = new PdfPCell(new Phrase("2", footblanco));
                cellitemenblanco2.HorizontalAlignment = Element.ALIGN_LEFT;
                cellitemenblanco2.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                cellitemenblanco2.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
                cellitemenblanco2.BorderWidthLeft = .8f;

                cellitemenblanco2.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
                cellitemenblanco2.BorderWidthRight = .8f;

                Conformidad.AddCell(cellitemenblanco2);

                PdfPCell cellitem = new PdfPCell(new Phrase("Recibi Conforme", piepag));
                cellitem.HorizontalAlignment = Element.ALIGN_CENTER;
                cellitem.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;

                cellitem.BorderColorLeft = new BaseColor(System.Drawing.Color.Black);
                cellitem.BorderWidthLeft = .8f;

                cellitem.BorderColorRight = new BaseColor(System.Drawing.Color.Black);
                cellitem.BorderWidthRight = .8f;

                cellitem.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                cellitem.BorderWidthBottom = .8f;

                Conformidad.AddCell(cellitem);
            }

            pdfDoc.Add(Conformidad);


            PdfContentByte cb = new PdfContentByte(writer);
            byte[] pdfbytes = PdfData.ToArray();

            pdfDoc.Close();

            //  return (pdfbytes);
            System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/GuiaRecepcion/" + NumDocumento + ".pdf"), PdfData.GetBuffer());
            return NumDocumento;
            // DownloadPDF(PdfData, NumDocumento);
        }

        #region--Download PDF
        public void DownloadPDF(System.IO.MemoryStream PDFData, string numdocumento)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.Charset = String.Empty;
            HttpContext.Current.Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + numdocumento + ".pdf"));
            HttpContext.Current.Response.OutputStream.Write(PDFData.GetBuffer(), 0, PDFData.GetBuffer().Length);
            HttpContext.Current.Response.OutputStream.Flush();
            HttpContext.Current.Response.OutputStream.Close();
            HttpContext.Current.Response.End();
        }

        #endregion
    }

}