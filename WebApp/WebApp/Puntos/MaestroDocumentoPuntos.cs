﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using Entidades;
using Negocio;
using System.Text;
using iTextSharp.text.pdf.parser;
using System.Data;
using iTextSharp.text.html.simpleparser;
using System.IO;
using WebApp;

namespace WcfServicioPDF.GuiaRecepcion
{
    public class MaestroDocumentoPuntos
    {
        public static object PDFParser { get; private set; }

        public string GenerateInvoice(string dni)
        {
            var output = new MemoryStream();
            Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 30f, 0f);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            // Open the Document for writing
            MemoryStream PdfData = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, PdfData);
            pdfDoc.Open();

            //Negocio.brCotizacion objcotizacion = new Negocio.brCotizacion();
            //Int32 idcargo = Convert.ToInt32(TransactionID);
            //Entidades.Cargo CabCargo = new Entidades.Cargo();


            Entidades.PersonaView  maestro = new Entidades.PersonaView();
            List<Entidades.PersonaView> listadoproductos = new List<Entidades.PersonaView>();
            Negocio.Persona  objpersona = new Negocio.Persona();
            string tipo = "N";
            listadoproductos = objpersona.BusquedaRucPuntosMaestro(dni,tipo);
            maestro =  objpersona.TraerPersona(dni);
            
            //ESTILOS DE TEXTO
            var titleFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
            var titleFontBlue = FontFactory.GetFont("Arial", 14, Font.NORMAL, BaseColor.BLUE);
            var boldTableFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
            var bodyFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var bodyfuente = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var itemtablefuente = FontFactory.GetFont("ArialNarrow", 8, Font.NORMAL);
            var bodyfuentebold = FontFactory.GetFont("Verdana", 10, Font.BOLD);
            var fontdireccion = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var EmailFont = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLUE);
            var footblanco = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.WHITE);
            var piepag = FontFactory.GetFont("Arial", 8, Font.NORMAL);

 
            //string NumDocumento = CabCargo.numcargo;


            BaseColor TabelHeaderBackGroundColor = WebColors.GetRGBColor("#EEEEEE");

            PdfPTable headertable = new PdfPTable(2);
            headertable.HorizontalAlignment = 0;
            headertable.WidthPercentage = 100;
            headertable.SpacingAfter = 9;
            headertable.SetWidths(new float[] { 70f, 170f });  // then set the column's __relative__ widths             
            headertable.DefaultCell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;


            ////IMAGEN DE LOGO
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/imagenes/logo.png"));
            logo.ScaleToFit(155, 90);
            PdfPCell pdfCelllogo = new PdfPCell(logo);
            pdfCelllogo.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            //pdfCelllogo.Border = Rectangle.BOX;                
            pdfCelllogo.HorizontalAlignment = 0;
            pdfCelllogo.PaddingTop = 5f;
            pdfCelllogo.PaddingLeft = 15f;
            pdfCelllogo.PaddingRight = 15f;
            pdfCelllogo.PaddingBottom = 5f;

            headertable.AddCell(pdfCelllogo);

            {
                PdfPCell middlecell = new PdfPCell(new Phrase("RELACION DE DOCUMENTOS  DEL MAESTRO :"+ maestro.maestroobra, bodyFont));
                middlecell.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER ;
                middlecell.HorizontalAlignment = 0;
                middlecell.VerticalAlignment = 0;
                middlecell.PaddingLeft = 75f;
                middlecell.PaddingTop = 5f;
                headertable.AddCell(middlecell);
            }

            //{
            //    RoundRectangle rr = new RoundRectangle();
            //    PdfPTable nested = new PdfPTable(1);
            //    nested.DefaultCell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            //    //nested.DefaultCell.CellEvent = rr;
            //    nested.WidthPercentage = 100;
            //    nested.HorizontalAlignment = 0;
                
            // //   nested.SetWidths(new float[] { 1f });

            //    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("SANICENTER", itemtablefuente));
            //    nextPostCell1.Border = Rectangle.BOTTOM_BORDER;
            //    //nextPostCell1.CellEvent = rr;
            //    nextPostCell1.HorizontalAlignment = Element.ALIGN_CENTER;
            //    nested.AddCell(nextPostCell1);

            //    PdfPCell nextPostCell2 = new PdfPCell(new Phrase());
            //    nextPostCell2.Border =  Rectangle.BOTTOM_BORDER;
            //    //nextPostCell2.CellEvent = rr;
            //    nextPostCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            //    nested.AddCell(nextPostCell2);


            //    PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Cantidad", itemtablefuente));
            //    nextPostCell3.Border =  Rectangle.NO_BORDER;                
            //    nextPostCell3.HorizontalAlignment = Element.ALIGN_CENTER;
            //    nested.AddCell(nextPostCell3);


            //    //    //CUADRO NUM Y TIPO DE DOCUMENTO
            //    PdfPCell nesthousing = new PdfPCell(nested);
            //    nesthousing.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            //    //nesthousing.CellEvent = rr;
            //    nesthousing.PaddingRight = 5f;
            //    nesthousing.PaddingTop = 5f;
            //    nesthousing.PaddingLeft = 5f;
            //    nesthousing.PaddingBottom = 2f;


            //    headertable.AddCell(nesthousing);
            //}


            {
                PdfPTable nested = new PdfPTable(1);
                nested.DefaultCell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                nested.WidthPercentage = 9;
                nested.HorizontalAlignment = 0;
                pdfCelllogo.PaddingLeft = 2f;
                nested.SetWidths(new float[] { 5f });


                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                nesthousing.PaddingRight = 5f;
                nesthousing.PaddingTop = 5f;
                nesthousing.PaddingLeft = 45f;
                nesthousing.PaddingBottom = 2f;
                headertable.AddCell(nesthousing);

            }

            pdfDoc.Add(headertable);







            PdfPTable itemTable = new PdfPTable(6);

            itemTable.HorizontalAlignment = 0;
            itemTable.WidthPercentage = 100;
            itemTable.SetWidths(new float[] {2, 18, 18, 18,18,18 });  // then set the column's __relative__ widths
            itemTable.SpacingAfter = 5;
            itemTable.PaddingTop = 5f;
            itemTable.DefaultCell.Border = Rectangle.BOX;
            PdfPCell cell1 = new PdfPCell(new Phrase("N", boldTableFont));
            cell1.BackgroundColor = TabelHeaderBackGroundColor;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            itemTable.AddCell(cell1);
            PdfPCell cell2 = new PdfPCell(new Phrase("Tipo de Documento", boldTableFont));
            cell2.BackgroundColor = TabelHeaderBackGroundColor;
            cell2.HorizontalAlignment = 1;
            itemTable.AddCell(cell2);
            PdfPCell cell3 = new PdfPCell(new Phrase("Documento", boldTableFont));
            cell3.BackgroundColor = TabelHeaderBackGroundColor;
            cell3.HorizontalAlignment = Element.ALIGN_CENTER;
            itemTable.AddCell(cell3);
            PdfPCell cell4 = new PdfPCell(new Phrase("Fecha", boldTableFont));
            cell4.BackgroundColor = TabelHeaderBackGroundColor;
            cell4.HorizontalAlignment = Element.ALIGN_CENTER;
            itemTable.AddCell(cell4);

            PdfPCell cell5 = new PdfPCell(new Phrase("Importe", boldTableFont));
            cell5.BackgroundColor = TabelHeaderBackGroundColor;
            cell5.HorizontalAlignment = Element.ALIGN_CENTER;
            itemTable.AddCell(cell5);

            PdfPCell cell6 = new PdfPCell(new Phrase("Puntos", boldTableFont));
            cell6.BackgroundColor = TabelHeaderBackGroundColor;
            cell6.HorizontalAlignment = Element.ALIGN_CENTER;
            itemTable.AddCell(cell6);

            int indice = 0;
            foreach (Entidades.PersonaView row in listadoproductos)
            {
                indice = indice + 1;
                PdfPCell numberCell = new PdfPCell(new Phrase(Convert.ToString(indice), itemtablefuente));
                numberCell.HorizontalAlignment = Element.ALIGN_CENTER; ;
                numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                itemTable.AddCell(numberCell);

                PdfPCell nombrepcell = new PdfPCell(new Phrase(row.tipodocumento, itemtablefuente));
                nombrepcell.HorizontalAlignment = Element.ALIGN_CENTER;
                nombrepcell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(nombrepcell);

                PdfPCell cargocell = new PdfPCell(new Phrase(row.documento, itemtablefuente));
                cargocell.HorizontalAlignment = Element.ALIGN_CENTER;
                cargocell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(cargocell);

                PdfPCell sedecell = new PdfPCell(new Phrase(row.doc_fechaemision, itemtablefuente));
                sedecell.HorizontalAlignment = Element.ALIGN_CENTER;
                sedecell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(sedecell);

                PdfPCell importe = new PdfPCell(new Phrase(Convert.ToString(row.doc_importe), itemtablefuente));
                importe.HorizontalAlignment = Element.ALIGN_CENTER;
                importe.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(importe);

                PdfPCell puntos = new PdfPCell(new Phrase(Convert.ToString(row.totalpuntos), itemtablefuente));
                puntos.HorizontalAlignment = Element.ALIGN_CENTER;
                puntos.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER; ;
                itemTable.AddCell(puntos);
            }
            pdfDoc.Add(itemTable);




            PdfPTable FentregaTable = new PdfPTable(1);

            FentregaTable.HorizontalAlignment = 0;
            FentregaTable.WidthPercentage = 100;
            FentregaTable.SetWidths(new float[] {  15 });  // then set the column's __relative__ widths
            FentregaTable.SpacingAfter = 2;
            FentregaTable.PaddingTop = 5f;
            FentregaTable.DefaultCell.Border = Rectangle.NO_BORDER;
            //FentregaTable.DefaultCell.Border = Rectangle.BOX;
            //PdfPCell cellfentrega = new PdfPCell(new Phrase("N", boldTableFont));
            //cellfentrega.BackgroundColor = TabelHeaderBackGroundColor;
            //cellfentrega.HorizontalAlignment = Element.ALIGN_LEFT;
            //FentregaTable.AddCell(cellfentrega);


            //PdfPCell EntregaCell = new PdfPCell(new Phrase("Fecha :" + CabCargo.FechaRegistro.ToString("dd/MM/yyyy"), itemtablefuente));
            //EntregaCell.HorizontalAlignment = Element.ALIGN_RIGHT; ;
            //EntregaCell.Border = Rectangle.NO_BORDER;
            //FentregaTable.AddCell(EntregaCell);

            pdfDoc.Add(FentregaTable);

            PdfContentByte cb = new PdfContentByte(writer);
            byte[] pdfbytes = PdfData.ToArray();

            pdfDoc.Close();

            //  return (pdfbytes);
            System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/Puntos/"+dni+".pdf"), PdfData.GetBuffer());
            return "";
            // DownloadPDF(PdfData, NumDocumento);
        }

    }

}