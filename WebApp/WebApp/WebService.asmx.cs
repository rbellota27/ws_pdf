﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WcfServicioPDF.GuiaRecepcion;
using System.Runtime.InteropServices;
using WebApp.Cotizacion;

namespace WebApp
{
    /// <summary>
    /// Descripción breve de WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
   // [System.Runtime.InteropServices.ImportedFromTypeLib()]
    public class WebService : System.Web.Services.WebService
    {        
        public static object Resources { get; internal set; }

        [WebMethod]
        public string PDFGuiaRemision(string iddocumento)
        {
            GuiaRecepcionPDF generacionPDF = new GuiaRecepcionPDF();
          string numdocumento = generacionPDF.GenerateInvoice(iddocumento);            
            return numdocumento;
        }
        [WebMethod]
        public string PDFCotizacion(string iddocumento)
        {
            CotizacionPDF PDFCotizacion = new CotizacionPDF();
            string numdocumentocoti = PDFCotizacion.GeneratGenerarCotizacioneInvoice(iddocumento);
            return numdocumentocoti;            
        }
        [WebMethod]
        public string PDFCotizacionGlosa(string iddocumento)
        {
            CotizacionPDFGlosa PDFCotizacion = new CotizacionPDFGlosa();
            string numdocumentocoti = PDFCotizacion.GeneratGenerarCotizacioneInvoice1(iddocumento);
            return numdocumentocoti;            
        }
        [WebMethod]
        public string PDFCargo(string iddocumento)
        {
            CargoPDF cargoobj = new CargoPDF();
            string numdocumento = cargoobj.GenerateInvoice(iddocumento);
            return numdocumento;
        }

        //[WebMethod]
        //public string PDFPuntosMaestro(string dni)
        //{
            //CargoPDF cargoobj = new CargoPDF();
            //string numdocumento = cargoobj.GenerateInvoice(iddocumento);
            //return numdocumento;
        //}
        
        [WebMethod]
        public string PDFProductosCanje()
        {
            ProductosPuntos cargoobj = new ProductosPuntos();
            string numdocumento = cargoobj.GenerateInvoice();
            return numdocumento;
        }

        [WebMethod]
        public string PDFDocumentosMaestroCanje(string dni)
        {
            MaestroDocumentoPuntos cargoobj = new MaestroDocumentoPuntos();
            string numdocumento = cargoobj.GenerateInvoice(dni);
            return numdocumento;
        }

        [WebMethod]
        public string PDFProductosCanje_Appolo()
        {
            ProductosPuntos_Appolo cargoobj = new ProductosPuntos_Appolo();
            string numdocumento = cargoobj.GenerateInvoice();
            return numdocumento;
        }

        [WebMethod]
        public string PDFDocumentosMaestroCanje_Appolo(string dni)
        {
            MaestroDocumentoPuntos_Appolo cargoobj = new MaestroDocumentoPuntos_Appolo();
            string numdocumento = cargoobj.GenerateInvoice(dni);
            return numdocumento;
        }
    }
}
