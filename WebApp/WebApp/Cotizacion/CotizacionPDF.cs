﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using Entidades;
using Negocio;
using System.Text;
using iTextSharp.text.pdf.parser;
using System.Data;
using iTextSharp.text.html.simpleparser;
using System.IO;

namespace WebApp.Cotizacion
{
    public class CotizacionPDF
    {
        public string GeneratGenerarCotizacioneInvoice(string TransactionID)
        {
            var output = new MemoryStream();
            Document pdfDoc = new Document(PageSize.A4, 22f, 22f, 30f, 0f);

            // Open the Document for writing
            MemoryStream PdfData = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, PdfData);
            pdfDoc.Open();


            Negocio.DocumentosMercantiles reporte = new Negocio.DocumentosMercantiles();
            List<Entidades.Documento> cabecera = new List<Entidades.Documento>();
            cabecera = reporte.CabeceraCotizacion(Convert.ToInt32(TransactionID));

            List<Entidades.DetalleDocumento> detallescoti = new List<Entidades.DetalleDocumento>();
            detallescoti = reporte.DetalleCotizacion(Convert.ToInt32(TransactionID));

            Entidades.Documento observaciones = new Entidades.Documento();
            observaciones = reporte.ObservacionesGuiaRemision(Convert.ToInt32(TransactionID));
            string NumDocumento = cabecera[0].NroDocumento;
            string numero = cabecera[0].Serie + "-" + cabecera[0].Codigo;

            var verdanabold = FontFactory.GetFont("Verdana", 9, Font.BOLD);
            var verdana = FontFactory.GetFont("Verdana", 9, Font.NORMAL);
            var verdanaBlan = FontFactory.GetFont("Verdana", 9, BaseColor.WHITE);
            var arial = FontFactory.GetFont("Arial", 10, Font.NORMAL);
            var tahomabold = FontFactory.GetFont("Tahoma", 11, Font.BOLD);

            BaseColor TabelHeaderBackGroundColor = WebColors.GetRGBColor("#EEEEEE");

            PdfPTable headertable = new PdfPTable(3);
            headertable.HorizontalAlignment = 0;
            headertable.WidthPercentage = 100;
            //headertable.SetWidths(new float[] { 130f, 40f, 110f });  // then set the column's __relative__ widths             
            headertable.SetWidths(new float[] { 125f, 100f, 80f });  // then set the column's __relative__ widths
            headertable.DefaultCell.Border = Rectangle.NO_BORDER;

            ////IMAGEN DE LOGO
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/imagenes/logo.png"));
            logo.ScaleToFit(155, 110);
            PdfPCell pdfCelllogo = new PdfPCell(logo);
            pdfCelllogo.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
            //pdfCelllogo.Border = Rectangle.BOX;                
            pdfCelllogo.HorizontalAlignment = 0;
            pdfCelllogo.PaddingTop = 5f;
            pdfCelllogo.PaddingLeft = 20f;

            headertable.AddCell(pdfCelllogo);


            {
                PdfPCell middlecell = new PdfPCell();
                middlecell.Border = Rectangle.TOP_BORDER;
                headertable.AddCell(middlecell);
            }

            {
                RoundRectangle rr = new RoundRectangle();
                PdfPTable nested = new PdfPTable(1);
                nested.DefaultCell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                //nested.DefaultCell.CellEvent = rr;
                nested.WidthPercentage = 9;
                nested.HorizontalAlignment = 0;
                pdfCelllogo.PaddingLeft = 1f;
                pdfCelllogo.PaddingRight = 15f;
                nested.SetWidths(new float[] { 1f });

                PdfPCell nextPostCell1 = new PdfPCell(new Phrase("RUC:" + cabecera[0].Ruc, verdanabold));
                nextPostCell1.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                //nextPostCell1.CellEvent = rr;
                nextPostCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                nested.AddCell(nextPostCell1);

                PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Cotización", verdanabold));
                nextPostCell2.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                //nextPostCell2.CellEvent = rr;
                nextPostCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                nested.AddCell(nextPostCell2);


                PdfPCell nextPostCell3 = new PdfPCell(new Phrase(cabecera[0].NroDocumento, verdanabold));
                nextPostCell3.BorderColorTop = new BaseColor(System.Drawing.Color.Black);                
                //nextPostCell3.CellEvent = rr;
                nextPostCell3.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                nextPostCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                nested.AddCell(nextPostCell3);

                //    //CUADRO NUM Y TIPO DE DOCUMENTO
                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;                
                //nesthousing.CellEvent = rr;
                nesthousing.PaddingRight = 5f;
                nesthousing.PaddingTop = 5f;
                nesthousing.PaddingLeft = 5f;
                nesthousing.PaddingBottom = 2f;


                headertable.AddCell(nesthousing);
            }

            pdfDoc.Add(headertable);


            PdfPTable tabladireccion = new PdfPTable(3);
            tabladireccion.HorizontalAlignment = 0;
            tabladireccion.WidthPercentage = 100;            
            tabladireccion.SetWidths(new float[] { 125f, 100f, 50f });  // then set the column's __relative__ widths
            tabladireccion.DefaultCell.Border = Rectangle.NO_BORDER;

            {
                PdfPCell cellcabdirecsani = new PdfPCell(new Phrase(cabecera[0].tie_Direccion, arial));
                //AV.ALFREDO MENDIOLA NRO 965 – URB.PALAO
                cellcabdirecsani.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdirecsani.Border = Rectangle.LEFT_BORDER;
                cellcabdirecsani.PaddingLeft = 5f;
                tabladireccion.AddCell(cellcabdirecsani);

                PdfPCell cellcabdirecsani0 = new PdfPCell(new Phrase(cabecera[0].tie_Ubigeo, arial));
                cellcabdirecsani0.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdirecsani0.Border = Rectangle.NO_BORDER;
                tabladireccion.AddCell(cellcabdirecsani0);

                PdfPCell cellcabvac = new PdfPCell(new Phrase("", arial));
                cellcabvac.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabvac.Border = Rectangle.RIGHT_BORDER;
                tabladireccion.AddCell(cellcabvac);
            }
            {
                PdfPCell cellcabdirectelef = new PdfPCell(new Phrase(cabecera[0].tie_Telefono, arial));
                //AV.ALFREDO MENDIOLA NRO 965 – URB.PALAO
                cellcabdirectelef.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabdirectelef.Border = Rectangle.LEFT_BORDER;
                cellcabdirectelef.PaddingLeft = 5f;
                tabladireccion.AddCell(cellcabdirectelef);

                PdfPCell cellcabemail = new PdfPCell(new Phrase(cabecera[0].tie_Email, arial));
                cellcabemail.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabemail.Border = Rectangle.NO_BORDER;
                tabladireccion.AddCell(cellcabemail);


                PdfPCell cellcabvac1 = new PdfPCell(new Phrase("", arial));
                cellcabvac1.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabvac1.Border = Rectangle.RIGHT_BORDER;
                tabladireccion.AddCell(cellcabvac1);
            }


            pdfDoc.Add(tabladireccion);

            PdfPTable tablaprincipal = new PdfPTable(8);
            tablaprincipal.HorizontalAlignment = 0;
            tablaprincipal.WidthPercentage = 100;
            tablaprincipal.SpacingAfter = 2;
            tablaprincipal.SetWidths(new float[] { 15,24,14,24,16,24,18,29});  // then set the column's __relative__ widths
            tablaprincipal.DefaultCell.Border = Rectangle.BOX;

            {
                PdfPCell cellcabtipodoc = new PdfPCell(new Phrase("COTIZACION", tahomabold));
                cellcabtipodoc.BackgroundColor = BaseColor.WHITE;
                cellcabtipodoc.HorizontalAlignment = Element.ALIGN_CENTER;
                cellcabtipodoc.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cellcabtipodoc.Colspan = 8;
                tablaprincipal.AddCell(cellcabtipodoc);
            }
            
            //CABECERA DEL DOCUMENTO  - COTIZACION
            {
                PdfPCell cellaten = new PdfPCell(new Phrase("Atencion:", verdanabold));
                cellaten.BackgroundColor = BaseColor.WHITE;
                cellaten.HorizontalAlignment = Element.ALIGN_LEFT;
                cellaten.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellaten);

                PdfPCell cellatenBD = new PdfPCell(new Phrase(cabecera[0].senores, verdana));
                cellatenBD.BackgroundColor = BaseColor.WHITE;
                cellatenBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellatenBD.Border = Rectangle.NO_BORDER;
                cellatenBD.Colspan = 3;
                tablaprincipal.AddCell(cellatenBD);

                PdfPCell cellruc = new PdfPCell(new Phrase("Ruc:", verdanabold));
                cellruc.BackgroundColor = BaseColor.WHITE;
                cellruc.HorizontalAlignment = Element.ALIGN_LEFT;
                cellruc.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(cellruc);

                PdfPCell cellrucBD = new PdfPCell(new Phrase(cabecera[0].Ruc, verdana));
                cellrucBD.BackgroundColor = BaseColor.WHITE;
                cellrucBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellrucBD.Border = Rectangle.RIGHT_BORDER;
                cellrucBD.Colspan = 3;
                tablaprincipal.AddCell(cellrucBD);

            }

            {
                PdfPCell celldir = new PdfPCell(new Phrase("Direccion:", verdanabold));
                celldir.BackgroundColor = BaseColor.WHITE;
                celldir.HorizontalAlignment = Element.ALIGN_LEFT;
                celldir.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(celldir);

                PdfPCell celldirBD = new PdfPCell(new Phrase(cabecera[0].Direccion, verdana));
                celldirBD.BackgroundColor = BaseColor.WHITE;
                celldirBD.HorizontalAlignment = Element.ALIGN_LEFT;
                celldirBD.Border = Rectangle.NO_BORDER;
                celldirBD.Colspan = 3;
                tablaprincipal.AddCell(celldirBD);


                PdfPCell cellubigeo = new PdfPCell(new Phrase("Ubigeo:", verdanabold));
                cellubigeo.BackgroundColor = BaseColor.WHITE;
                cellubigeo.HorizontalAlignment = Element.ALIGN_LEFT;
                cellubigeo.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(cellubigeo);

                PdfPCell cellubigeoBD = new PdfPCell(new Phrase(cabecera[0].UbigeoCliente, verdana));
                cellubigeoBD.BackgroundColor = BaseColor.WHITE;
                cellubigeoBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellubigeoBD.Border = Rectangle.RIGHT_BORDER;
                cellubigeoBD.Colspan = 3;
                tablaprincipal.AddCell(cellubigeoBD);

            }


            {
                PdfPCell cellcabfe = new PdfPCell(new Phrase("Fecha:", verdanabold));
                cellcabfe.BackgroundColor = BaseColor.WHITE;
                cellcabfe.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabfe.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcabfe);

                PdfPCell cellcabfeBD = new PdfPCell(new Phrase(cabecera[0].FechaEmision.ToString("dd MMMMM, yyyy"), verdana));
                cellcabfeBD.BackgroundColor = BaseColor.WHITE;
                cellcabfeBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabfeBD.Border = Rectangle.NO_BORDER;
                cellcabfeBD.Colspan = 3;
                tablaprincipal.AddCell(cellcabfeBD);

                PdfPCell celltelf1 = new PdfPCell(new Phrase("Telefono1:", verdanabold));
                celltelf1.BackgroundColor = BaseColor.WHITE;
                celltelf1.HorizontalAlignment = Element.ALIGN_LEFT;
                celltelf1.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(celltelf1);

                PdfPCell celltelf1BD = new PdfPCell(new Phrase(cabecera[0].tel_Numero, verdana));
                celltelf1BD.BackgroundColor = BaseColor.WHITE;
                celltelf1BD.HorizontalAlignment = Element.ALIGN_LEFT;
                celltelf1BD.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(celltelf1BD);

                PdfPCell celltelf2 = new PdfPCell(new Phrase("Telefono2:", verdanabold));
                celltelf2.BackgroundColor = BaseColor.WHITE;
                celltelf2.HorizontalAlignment = Element.ALIGN_LEFT;
                celltelf2.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(celltelf2);

                PdfPCell celltelf2BD = new PdfPCell(new Phrase(cabecera[0].Celular, verdana));
                celltelf2BD.BackgroundColor = BaseColor.WHITE;
                celltelf2BD.HorizontalAlignment = Element.ALIGN_LEFT;
                celltelf2BD.Border = Rectangle.RIGHT_BORDER;
                tablaprincipal.AddCell(celltelf2BD);
            }
            {

                PdfPCell cellcacont = new PdfPCell(new Phrase("Contacto:", verdanabold));
                cellcacont.BackgroundColor = BaseColor.WHITE;
                cellcacont.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcacont.Border = Rectangle.LEFT_BORDER;
                tablaprincipal.AddCell(cellcacont);

                PdfPCell cellcabcontBD = new PdfPCell(new Phrase(cabecera[0].Contacto, verdana));
                cellcabcontBD.BackgroundColor = BaseColor.WHITE;
                cellcabcontBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabcontBD.Border = Rectangle.NO_BORDER;
                cellcabcontBD.Colspan = 3;
                tablaprincipal.AddCell(cellcabcontBD);

                PdfPCell celldirent = new PdfPCell(new Phrase("Direccion Entrega:", verdanabold));
                celldirent.BackgroundColor = BaseColor.WHITE;
                celldirent.HorizontalAlignment = Element.ALIGN_LEFT;
                celldirent.Border = Rectangle.NO_BORDER;
                celldirent.Colspan = 2;
                tablaprincipal.AddCell(celldirent);

                PdfPCell celldirentBD = new PdfPCell(new Phrase(cabecera[0].pll_Direccion, verdana));
                celldirentBD.BackgroundColor = BaseColor.WHITE;
                celldirentBD.HorizontalAlignment = Element.ALIGN_LEFT;
                celldirentBD.Border = Rectangle.RIGHT_BORDER;
                celldirentBD.Colspan = 2;
                tablaprincipal.AddCell(celldirentBD);
            }
            {
                PdfPCell cellcabtien = new PdfPCell(new Phrase("Tienda:", verdanabold));
                cellcabtien.BackgroundColor = BaseColor.WHITE;
                cellcabtien.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabtien.Border = Rectangle.LEFT_BORDER;                
                tablaprincipal.AddCell(cellcabtien);

                PdfPCell cellcabtiendBD = new PdfPCell(new Phrase(cabecera[0].tie_Nombre, arial));
                cellcabtiendBD.BackgroundColor = BaseColor.WHITE;
                cellcabtiendBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabtiendBD.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(cellcabtiendBD);

                PdfPCell cellcabemp = new PdfPCell(new Phrase("Empresa:", verdanabold));
                cellcabemp.BackgroundColor = BaseColor.WHITE;
                cellcabemp.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabemp.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(cellcabemp);

                PdfPCell cellcabempBD = new PdfPCell(new Phrase(cabecera[0].TelfCliente, arial));
                cellcabempBD.BackgroundColor = BaseColor.WHITE;
                cellcabempBD.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabempBD.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(cellcabempBD);

                PdfPCell callcabven = new PdfPCell(new Phrase("Vendedor:", verdanabold));
                callcabven.BackgroundColor = BaseColor.WHITE;
                callcabven.HorizontalAlignment = Element.ALIGN_LEFT;
                callcabven.Border = Rectangle.NO_BORDER;
                tablaprincipal.AddCell(callcabven);

                PdfPCell callcabvenBD = new PdfPCell(new Phrase(cabecera[0].Vendedor, arial));
                callcabvenBD.BackgroundColor = BaseColor.WHITE;
                callcabvenBD.HorizontalAlignment = Element.ALIGN_LEFT;
                callcabvenBD.Border = Rectangle.RIGHT_BORDER;
                callcabvenBD.Colspan = 3;
                tablaprincipal.AddCell(callcabvenBD);
            }

            {
                PdfPCell cellcabpiecab = new PdfPCell(new Phrase("Por medio de la presente sometemos a su consideración los siguientes productos:", arial));
                cellcabpiecab.BackgroundColor = BaseColor.WHITE;
                cellcabpiecab.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcabpiecab.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER  | Rectangle.RIGHT_BORDER;
                cellcabpiecab.Colspan = 8;
                tablaprincipal.AddCell(cellcabpiecab);
            }

            pdfDoc.Add(tablaprincipal);


            PdfPTable cabitemTable = new PdfPTable(6);

            cabitemTable.HorizontalAlignment = 0;
            cabitemTable.WidthPercentage = 100;
            cabitemTable.SetWidths(new float[] { 13, 60, 8, 12, 8, 25 });  // then set the column's __relative__ widths
            cabitemTable.SpacingAfter = 2;
            cabitemTable.PaddingTop = 5f;
            cabitemTable.DefaultCell.Border = Rectangle.BOX;

            PdfPCell cell1 = new PdfPCell(new Phrase("Cód.", verdana));
            cell1.BackgroundColor = BaseColor.WHITE;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            cabitemTable.AddCell(cell1);
            PdfPCell cell3 = new PdfPCell(new Phrase("Descripcion", verdana));
            cell3.BackgroundColor = BaseColor.WHITE;
            cell3.HorizontalAlignment = Element.ALIGN_LEFT;
            cabitemTable.AddCell(cell3);
            PdfPCell cell4 = new PdfPCell(new Phrase("UM", verdana));
            cell4.BackgroundColor = BaseColor.WHITE;
            cell4.HorizontalAlignment = Element.ALIGN_LEFT;
            cabitemTable.AddCell(cell4);
            PdfPCell cell5 = new PdfPCell(new Phrase("Cantidad", verdana));
            cell5.BackgroundColor = BaseColor.WHITE;
            cell5.HorizontalAlignment = Element.ALIGN_RIGHT;
            cabitemTable.AddCell(cell5);
            PdfPCell cell6 = new PdfPCell(new Phrase("Precio", verdana));
            cell6.BackgroundColor = BaseColor.WHITE;
            cell6.HorizontalAlignment = Element.ALIGN_RIGHT;
            cabitemTable.AddCell(cell6);
            PdfPCell cell7 = new PdfPCell(new Phrase("Importe", verdana));
            cell7.BackgroundColor = BaseColor.WHITE;
            cell7.HorizontalAlignment = Element.ALIGN_RIGHT;
            cabitemTable.AddCell(cell7);

            pdfDoc.Add(cabitemTable);




            PdfPTable itemTable = new PdfPTable(6);
            itemTable.HorizontalAlignment = 0;
            itemTable.WidthPercentage = 100;
            itemTable.SetWidths(new float[] { 13, 60, 8, 12, 8, 25 });  // then set the column's __relative__ widths            
            itemTable.PaddingTop = 2f;
            itemTable.DefaultCell.Border = Rectangle.BOX;
            int indice = 0;

            foreach (Entidades.DetalleDocumento row in detallescoti)
            {
                indice = indice + 1;
                PdfPCell numberCell = new PdfPCell(new Phrase(row.CodigoProducto, verdana));
                numberCell.HorizontalAlignment = Element.ALIGN_LEFT;                
                if (indice == 1)
                {                    
                    numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER; 
                }
                else
                {
                    numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                }

                itemTable.AddCell(numberCell);

                PdfPCell Codigocell = new PdfPCell(new Phrase(row.NomProducto, verdana));
                Codigocell.HorizontalAlignment = Element.ALIGN_LEFT;

                if (indice == 1)
                {
                    Codigocell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
                }
                else
                {
                    Codigocell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                }

                itemTable.AddCell(Codigocell);

                PdfPCell descCell = new PdfPCell(new Phrase(row.UmPeso, verdana));
                descCell.HorizontalAlignment = Element.ALIGN_LEFT;                
                
                if (indice == 1)
                {
                    descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
                }
                else
                {
                    descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                }
                itemTable.AddCell(descCell);

                decimal cantidad = 0;
                cantidad = Math.Round(row.Cantidad, 2);
                PdfPCell amountCell = new PdfPCell(new Phrase(Convert.ToString(cantidad), verdana));
                amountCell.HorizontalAlignment = Element.ALIGN_RIGHT;                

                if (indice == 1)
                {
                    amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
                }
                else
                {
                    amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                }

                itemTable.AddCell(amountCell);

                decimal precio = 0;
                precio = Math.Round(row.PrecioCD, 2);
                PdfPCell totalamtCell = new PdfPCell(new Phrase(Convert.ToString(precio), verdana));
                totalamtCell.HorizontalAlignment = Element.ALIGN_RIGHT;

                if (indice == 1)
                {
                    totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
                }
                else
                {
                    totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                }
                itemTable.AddCell(totalamtCell);

                decimal importe = 0;
                importe = Math.Round(row.Importe, 2);
                PdfPCell totalimpCell = new PdfPCell(new Phrase(Convert.ToString(importe), verdana));
                totalimpCell.HorizontalAlignment = Element.ALIGN_RIGHT;                

                if (indice == 1)
                {
                    totalimpCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
                }
                else
                {
                    totalimpCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                }

                itemTable.AddCell(totalimpCell);
            } 


            {
                PdfPCell celltotal = new PdfPCell(new Phrase("Total:", verdanabold));
                celltotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celltotal.Border = Rectangle.LEFT_BORDER;
                celltotal.Colspan = 5;
                celltotal.PaddingBottom = 2;
                itemTable.AddCell(celltotal);

                PdfPCell celltotalvalor = new PdfPCell();                
                celltotal.Border = Rectangle.BOX;

                PdfPTable totalvalorconsimbolo = new PdfPTable(2);
                totalvalorconsimbolo.HorizontalAlignment = 0;
                totalvalorconsimbolo.WidthPercentage = 100;
                totalvalorconsimbolo.DefaultCell.Border = Rectangle.NO_BORDER;
                totalvalorconsimbolo.SetWidths(new float[] { 12, 70 });

                {
                    PdfPCell celltotalvalor1 = new PdfPCell(new Phrase(Convert.ToString(cabecera[0].mon_Simbolo), verdanabold));
                    celltotalvalor1.HorizontalAlignment = Element.ALIGN_LEFT;
                    celltotalvalor1.Border = Rectangle.NO_BORDER;
                    totalvalorconsimbolo.AddCell(celltotalvalor1);

                    decimal totalcab = 0;
                    totalcab = Math.Round(cabecera[0].Total, 2);
                    PdfPCell celltotalvalor2 = new PdfPCell(new Phrase(Convert.ToString(totalcab), verdana));
                    celltotalvalor2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celltotalvalor2.Border = Rectangle.NO_BORDER;
                    totalvalorconsimbolo.AddCell(celltotalvalor2);
                }
                celltotal.AddElement(totalvalorconsimbolo);
                itemTable.AddCell(celltotal);

            }

            {
                PdfPCell cellimporte = new PdfPCell(new Phrase("Importe Venta:", verdanabold));
                cellimporte.HorizontalAlignment = Element.ALIGN_RIGHT;
                cellimporte.Border = Rectangle.LEFT_BORDER;
                cellimporte.Colspan = 5;
                cellimporte.PaddingBottom = 2;
                itemTable.AddCell(cellimporte);
                
                
                PdfPCell cellimportevalor = new PdfPCell();
                cellimportevalor.Border = Rectangle.BOX;

                PdfPTable importeconsimbolo = new PdfPTable(2);
                importeconsimbolo.HorizontalAlignment = 0;
                importeconsimbolo.WidthPercentage = 100;
                importeconsimbolo.DefaultCell.Border = Rectangle.NO_BORDER;
                importeconsimbolo.SetWidths(new float[] { 12, 70 });

                {
                    PdfPCell cellimportevalor1 = new PdfPCell(new Phrase(Convert.ToString(cabecera[0].mon_Simbolo), verdanabold));
                    cellimportevalor1.HorizontalAlignment = Element.ALIGN_LEFT;
                    cellimportevalor1.Border = Rectangle.NO_BORDER;
                    importeconsimbolo.AddCell(cellimportevalor1);

                    decimal subtotalcab = 0;
                    subtotalcab = Math.Round(cabecera[0].SubTotal, 2);
                    PdfPCell cellimportevalor2 = new PdfPCell(new Phrase(Convert.ToString(subtotalcab), verdana));
                    cellimportevalor2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cellimportevalor2.Border = Rectangle.NO_BORDER;
                    importeconsimbolo.AddCell(cellimportevalor2);
                }
                cellimportevalor.AddElement(importeconsimbolo);
                itemTable.AddCell(cellimportevalor);

            }


            {
                PdfPCell celligv = new PdfPCell(new Phrase("I.G.V.", verdanabold));
                celligv.HorizontalAlignment = Element.ALIGN_RIGHT;
                celligv.Border = Rectangle.LEFT_BORDER;
                celligv.Colspan = 5;
                celligv.PaddingBottom = 2;
                itemTable.AddCell(celligv);



                PdfPCell celligvvalor = new PdfPCell();
                celligvvalor.Border = Rectangle.BOX;

                PdfPTable igvconsimbolo = new PdfPTable(2);
                igvconsimbolo.HorizontalAlignment = 0;
                igvconsimbolo.WidthPercentage = 100;
                igvconsimbolo.DefaultCell.Border = Rectangle.NO_BORDER;
                igvconsimbolo.SetWidths(new float[] { 12, 70 });
                {
                    PdfPCell celligvvalor1 = new PdfPCell(new Phrase(Convert.ToString(cabecera[0].mon_Simbolo), verdanabold));
                    celligvvalor1.HorizontalAlignment = Element.ALIGN_LEFT;
                    celligvvalor1.Border = Rectangle.NO_BORDER;
                    igvconsimbolo.AddCell(celligvvalor1);

                    decimal igvcab = 0;
                    igvcab = Math.Round(cabecera[0].IGV, 2);
                    PdfPCell celligvvalor2 = new PdfPCell(new Phrase(Convert.ToString(igvcab), verdana));
                    celligvvalor2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celligvvalor2.Border = Rectangle.NO_BORDER;
                    igvconsimbolo.AddCell(celligvvalor2);
                }
                celligvvalor.AddElement(igvconsimbolo);
                itemTable.AddCell(celligvvalor);

            }

            {
                PdfPCell cellimportePercep = new PdfPCell(new Phrase("Importe de la Percepccion", verdanabold));
                cellimportePercep.HorizontalAlignment = Element.ALIGN_RIGHT;
                cellimportePercep.Border = Rectangle.LEFT_BORDER;
                cellimportePercep.Colspan = 5;
                cellimportePercep.PaddingBottom = 2;
                itemTable.AddCell(cellimportePercep);

                PdfPCell cellimportePercepvalor = new PdfPCell();
                cellimportePercepvalor.Border = Rectangle.BOX;

                PdfPTable percepcionconsimbolo = new PdfPTable(2);
                percepcionconsimbolo.HorizontalAlignment = 0;
                percepcionconsimbolo.WidthPercentage = 100;
                percepcionconsimbolo.DefaultCell.Border = Rectangle.NO_BORDER;
                percepcionconsimbolo.SetWidths(new float[] { 12, 70 });

                {
                    PdfPCell cellimportePercepvalor1 = new PdfPCell(new Phrase(Convert.ToString(cabecera[0].mon_Simbolo), verdanabold));
                    cellimportePercepvalor1.HorizontalAlignment = Element.ALIGN_LEFT;
                    cellimportePercepvalor1.Border = Rectangle.NO_BORDER;
                    percepcionconsimbolo.AddCell(cellimportePercepvalor1);


                    decimal percepcioncab = 0;
                    percepcioncab = Math.Round(cabecera[0].Percepcion, 2);
                    PdfPCell cellimportePercepvalor2 = new PdfPCell(new Phrase(Convert.ToString(percepcioncab), verdana));
                    cellimportePercepvalor2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cellimportePercepvalor2.Border = Rectangle.NO_BORDER;
                    percepcionconsimbolo.AddCell(cellimportePercepvalor2);
                }
                cellimportePercepvalor.AddElement(percepcionconsimbolo);
                itemTable.AddCell(cellimportePercepvalor);

            }

            {
                PdfPCell celltotalapagar = new PdfPCell(new Phrase("Total a Pagar:", verdanabold));
                celltotalapagar.HorizontalAlignment = Element.ALIGN_RIGHT;
                celltotalapagar.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER;
                celltotalapagar.Colspan = 5;
                celltotalapagar.PaddingBottom = 2;
                itemTable.AddCell(celltotalapagar);

                
                PdfPCell celltotalapagarvalor = new PdfPCell();
                celltotalapagarvalor.Border = Rectangle.BOX;
                

                PdfPTable totalesconsimbolo = new PdfPTable(2);
                totalesconsimbolo.HorizontalAlignment = 0;
                totalesconsimbolo.WidthPercentage = 100;
                totalesconsimbolo.DefaultCell.Border = Rectangle.NO_BORDER;
                totalesconsimbolo.SetWidths(new float[] { 12, 70});
                {
                    
                    PdfPCell celltotalapagarvalor1 = new PdfPCell(new Phrase(Convert.ToString(cabecera[0].mon_Simbolo), verdanabold));
                    celltotalapagarvalor1.HorizontalAlignment = Element.ALIGN_LEFT;
                    celltotalapagarvalor1.Border = Rectangle.NO_BORDER;
                    totalesconsimbolo.AddCell(celltotalapagarvalor1);

                    decimal totalapagarcab = 0;
                    totalapagarcab = Math.Round(cabecera[0].TotalAPagar, 2);                    
                    PdfPCell celltotalapagarvalor2 = new PdfPCell(new Phrase(Convert.ToString(totalapagarcab), verdana));
                    celltotalapagarvalor2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celltotalapagarvalor2.Border = Rectangle.NO_BORDER;
                    totalesconsimbolo.AddCell(celltotalapagarvalor2);
                }
                celltotalapagarvalor.AddElement(totalesconsimbolo);
                itemTable.AddCell(celltotalapagarvalor);
            }

            {
                PdfPCell cellcondicionesgenerales = new PdfPCell(new Phrase(" Condiciones Comerciales:", verdanabold));
                cellcondicionesgenerales.HorizontalAlignment = Element.ALIGN_LEFT;
                cellcondicionesgenerales.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                cellcondicionesgenerales.Colspan = 6;
                itemTable.AddCell(cellcondicionesgenerales);

                PdfPCell cellconcgenertexto1 = new PdfPCell(new Phrase(" * Precios unitarios sujetos a variacion sin previo aviso.", verdana));
                cellconcgenertexto1.HorizontalAlignment = Element.ALIGN_LEFT;
                cellconcgenertexto1.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cellconcgenertexto1.Colspan = 6;
                itemTable.AddCell(cellconcgenertexto1);

                PdfPCell cellconcgenertexto2 = new PdfPCell(new Phrase(" * No Incluye costo por transporte.", verdana));
                cellconcgenertexto2.HorizontalAlignment = Element.ALIGN_LEFT;
                cellconcgenertexto2.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cellconcgenertexto2.Colspan = 6;
                itemTable.AddCell(cellconcgenertexto2);

                PdfPCell cellconcgenertexto3 = new PdfPCell(new Phrase(" * Favor de confirmar su pedido referenciando a esta cotizacion en su orden de compra.", verdana));
                cellconcgenertexto3.HorizontalAlignment = Element.ALIGN_LEFT;
                cellconcgenertexto3.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cellconcgenertexto3.Colspan = 6;
                itemTable.AddCell(cellconcgenertexto3);


                PdfPCell cellconcgenertexto4 = new PdfPCell(new Phrase(" * Entrega inmediata, salvo previa venta.", verdana));
                cellconcgenertexto4.HorizontalAlignment = Element.ALIGN_LEFT;
                cellconcgenertexto4.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                cellconcgenertexto4.Colspan = 6;
                itemTable.AddCell(cellconcgenertexto4);
            }


            {
                PdfPCell cellobservaciones = new PdfPCell(new Phrase(" Observaciones:", verdanabold));
                cellobservaciones.BackgroundColor = BaseColor.WHITE;
                cellobservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                cellobservaciones.Border = Rectangle.BOX;
                cellobservaciones.Colspan = 6;
                itemTable.AddCell(cellobservaciones);
            }
            {
                if (observaciones.observacion == "" || observaciones.observacion == null)
                {
                    PdfPCell cellitemobser = new PdfPCell(new Phrase("..................", verdanaBlan));
                    cellitemobser.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellitemobser.Colspan = 6;
                    cellitemobser.BackgroundColor = BaseColor.WHITE;
                    cellitemobser.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.TOP_BORDER;
                    itemTable.AddCell(cellitemobser);

                    PdfPCell cellitemobser1 = new PdfPCell(new Phrase("..................", verdanaBlan));
                    cellitemobser1.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellitemobser1.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    cellitemobser1.BackgroundColor = BaseColor.WHITE;
                    cellitemobser1.Colspan = 6;
                    itemTable.AddCell(cellitemobser1);
                }
                else
                {
                    PdfPCell cellitemobser = new PdfPCell(new Phrase(observaciones.observacion, verdana));
                    cellitemobser.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellitemobser.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    cellitemobser.Colspan = 6;
                    itemTable.AddCell(cellitemobser);
                }
            }

            {
                PdfPCell cellaceptacion = new PdfPCell(new Phrase(" Aceptacion:", verdanabold));
                cellaceptacion.BackgroundColor = BaseColor.WHITE;
                cellaceptacion.HorizontalAlignment = Element.ALIGN_LEFT;
                cellaceptacion.Border = Rectangle.BOX;
                cellaceptacion.Colspan = 6;
                itemTable.AddCell(cellaceptacion);
            }
            {
                PdfPCell cellitemacepsani = new PdfPCell(new Phrase("POR SANICENTER", verdana));
                cellitemacepsani.HorizontalAlignment = Element.ALIGN_CENTER;
                cellitemacepsani.Colspan = 3;
                cellitemacepsani.Border = Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
                itemTable.AddCell(cellitemacepsani);


                PdfPCell cellitemacepclie = new PdfPCell(new Phrase("POR CLIENTE", verdana));
                cellitemacepclie.HorizontalAlignment = Element.ALIGN_CENTER;
                cellitemacepclie.Colspan = 3;
                cellitemacepclie.Border = Rectangle.RIGHT_BORDER | Rectangle.TOP_BORDER;
                itemTable.AddCell(cellitemacepclie);
            }

            {
                PdfPCell cellitemacepespacio = new PdfPCell(new Phrase("..................", verdanaBlan));
                cellitemacepespacio.HorizontalAlignment = Element.ALIGN_CENTER;
                cellitemacepespacio.Colspan = 6;
                cellitemacepespacio.BackgroundColor = BaseColor.WHITE;
                cellitemacepespacio.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                itemTable.AddCell(cellitemacepespacio);
            }

            {
                PdfPCell cellitemacepfirmsani = new PdfPCell(new Phrase("", verdana));
                cellitemacepfirmsani.HorizontalAlignment = Element.ALIGN_CENTER;
                cellitemacepfirmsani.Colspan = 3;
                cellitemacepfirmsani.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER;
                itemTable.AddCell(cellitemacepfirmsani);


                PdfPCell cellitemacepclie = new PdfPCell(new Phrase("" + cabecera[0].senores + "", verdana));
                cellitemacepclie.HorizontalAlignment = Element.ALIGN_CENTER;
                cellitemacepclie.Colspan = 3;
                cellitemacepclie.Border = Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                itemTable.AddCell(cellitemacepclie);
            }
            pdfDoc.Add(itemTable);


            //LINEA PRODUCTOS SUJETO DE PERCEPCION

            PdfPTable PIETABLE = new PdfPTable(1);
            PIETABLE.HorizontalAlignment = 0;
            PIETABLE.WidthPercentage = 100;
            PIETABLE.DefaultCell.Border = Rectangle.NO_BORDER  ;

            {
                PdfPCell cellpiepagina = new PdfPCell(new Phrase("** Productos sujeto de percepcion", verdana));
                cellpiepagina.BackgroundColor = BaseColor.WHITE;
                cellpiepagina.HorizontalAlignment = Element.ALIGN_LEFT;
                cellpiepagina.Border = Rectangle.TOP_BORDER;
                cellpiepagina.Colspan = 6;
                PIETABLE.AddCell(cellpiepagina);
            }

            pdfDoc.Add(PIETABLE);


            PdfContentByte cb = new PdfContentByte(writer);
            byte[] pdfbytes = PdfData.ToArray();

            pdfDoc.Close();

            //  return (pdfbytes);

            System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/Cotizacion/" + numero + ".pdf"), PdfData.GetBuffer());
            return numero;
            //DownloadPDF(PdfData, "a");
        }


        #region--Download PDF
        public void DownloadPDF(System.IO.MemoryStream PDFData, string numdocumento)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.Charset = String.Empty;
            HttpContext.Current.Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + numdocumento + ".pdf"));
            HttpContext.Current.Response.OutputStream.Write(PDFData.GetBuffer(), 0, PDFData.GetBuffer().Length);
            HttpContext.Current.Response.OutputStream.Flush();
            HttpContext.Current.Response.OutputStream.Close();
            HttpContext.Current.Response.End();
        }

        #endregion
        public void cellLayout(PdfPCell cell, Rectangle rect,
                PdfContentByte[] canvas)
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.RoundRectangle(
                rect.GetLeft(1.5f), rect.GetBottom(1.5f), rect.GetRight(-3),
                rect.GetTop(-3), 4);
            cb.Stroke();
        }
    }
}
