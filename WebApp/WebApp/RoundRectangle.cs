﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using Entidades;
using Negocio;
using System.Text;
using iTextSharp.text.pdf.parser;
using System.Data;
using iTextSharp.text.html.simpleparser;
using System.IO;

namespace WebApp
{
    //class RoundRectangle private PdfPCellEvent {
    //    public void cellLayout(PdfPCell cell, Rectangle rect,
    //        PdfContentByte[] canvas)
    //    {
    //    PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
    //    cb.RoundRectangle(
    //        rect.GetLeft(1.5f), rect.GetBottom(1.5f), rect.GetRight(-3),
    //        rect.GetTop(-3), 4);
    //    cb.Stroke();
    //    }
    //}

    public class RoundRectangle : IPdfPCellEvent
    {
        public void CellLayout(
          PdfPCell cell, Rectangle rect, PdfContentByte[] canvas
        )
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.RoundRectangle(
              rect.Left,
              rect.Bottom,
              rect.Width,
              rect.Height,
              4 // change to adjust how "round" corner is displayed
            );
            cb.SetLineWidth(1f);
            cb.SetCMYKColorStrokeF(0f, 0f, 0f, 1f);
            cb.Stroke();
        }
    }
}
